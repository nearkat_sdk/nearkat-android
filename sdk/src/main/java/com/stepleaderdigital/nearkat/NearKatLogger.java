package com.stepleaderdigital.nearkat;

import android.util.Log;

public class NearKatLogger {
    
    private static final String LOG_TAG               = "NEARKAT";
    private static final int    STACK_TRACE_LEVELS_UP = 5;
    
    public static void v(Object object) {
        if (NearKat.getInstance().getIsDebug()||true) {
            Log.v(LOG_TAG, getClassNameMethodNameAndLineNumber() + object);
        }
    }
    
    public static void v() {
        if (NearKat.getInstance().getIsDebug()||true) {
            Log.v(LOG_TAG, getClassNameMethodNameAndLineNumber());
        }
    }
    
    private static int getLineNumber() {
        return Thread.currentThread().getStackTrace()[STACK_TRACE_LEVELS_UP].getLineNumber();
    }
    
    private static String getClassName() {
        String fileName = Thread.currentThread().getStackTrace()[STACK_TRACE_LEVELS_UP].getFileName();
        return fileName.substring(0, fileName.length() - 5);
    }
    
    private static String getMethodName() {
        return Thread.currentThread().getStackTrace()[STACK_TRACE_LEVELS_UP].getMethodName();
    }
    
    private static String getClassNameMethodNameAndLineNumber() {
        return "[" + getClassName() + "." + getMethodName() + "()-" + getLineNumber() + "]: ";
    }
}
