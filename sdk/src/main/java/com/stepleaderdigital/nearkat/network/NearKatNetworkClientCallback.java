package com.stepleaderdigital.nearkat.network;

import org.json.JSONObject;

public interface NearKatNetworkClientCallback {
    public void onSuccess(JSONObject response);
    public void onFailure(String response);
}
