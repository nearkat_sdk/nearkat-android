package com.stepleaderdigital.nearkat.network;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import com.stepleaderdigital.nearkat.NearKatLogger;
import com.stepleaderdigital.nearkat.NearKat;
import com.stepleaderdigital.nearkat.Utils;
import com.stepleaderdigital.nearkat.model.NearKatBeacon;

import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NearKatNetworkClient {
    
    public static final String   SEGMENT_INFO   = "/api/v2/info";
    public static final String   SEGMENT_BEACON = "/api/v2/event/beacon";
    public static final String   SEGMENT_APP_INFO  = "/api/v2/info/app";
    
    private static final ExecutorService      executor       = Executors.newFixedThreadPool(3);

    public static void registerDevice(final Context context, final NearKatNetworkClientCallback callback) {
        String json = Utils.getJsonInitInfo(context);
        execute(NearKatNetworkClient.SEGMENT_INFO, json, callback);
    }

    public static void sendInstalledApps(final Context context, final List<String> apps, final NearKatNetworkClientCallback callback){
        String json = Utils.getJsonAppInfo(context, apps);
        execute(NearKatNetworkClient.SEGMENT_APP_INFO, json, callback);
    }

    public static void sendNotificationOfBeacon(final Context context, NearKatBeacon beacon, final NearKatNetworkClientCallback callback) {
        String json = Utils.getJsonBeacon(context, beacon);
        execute(NearKatNetworkClient.SEGMENT_BEACON, json, callback);
    }

    private static void execute(final String segment, final String json, final NearKatNetworkClientCallback callback) {
        NearKatLogger.v("NearKat service call made to segment " + segment + ", callback: " + callback + ", json: " + json);
        final Handler handler = new Handler(Looper.getMainLooper());
        executor.execute(new NearKatNetworkClient.Task(handler, segment, json, callback));
    }
    
    private static class Task implements Runnable {
        
        private Handler               handler  = null;
        private String                segment  = null;
        private String                json     = null;
        private NearKatNetworkClientCallback callback = null;
        
        public Task(Handler handler, String segment, String json, NearKatNetworkClientCallback callback) {
            this.handler = handler;
            this.segment = segment;
            this.json = json;
            this.callback = callback;
        }
        
        @Override
        public void run() {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
            try {
                URL link = new URL(NearKat.getInstance().getAPIBaseURL() + segment);
                HttpURLConnection urlConnection = (HttpURLConnection) link.openConnection();
                
                urlConnection.addRequestProperty("X-API-KEY", NearKat.getInstance().getAPIKey());
                urlConnection.addRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestProperty("Connection", "keep-alive");
                urlConnection.setRequestProperty("Connection", "close");
                urlConnection.setRequestMethod("POST");
                
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                
                OutputStream os = urlConnection.getOutputStream();
                os.write(json.getBytes(HTTP.UTF_8));
                os.close();
                
                int responseCode = urlConnection.getResponseCode();
                
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder total = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    total.append(line);
                }
                String content = total.toString();
                in.close();

                if (responseCode == HttpURLConnection.HTTP_OK) {
                    JSONObject jsonObject = new JSONObject(content);
                    success(callback, jsonObject);
                } else {
                    failed(callback, content);
                }
                
            } catch (final Exception ex) {
                failed(callback, ex.toString());
            }
            
        }
        
        private void success(final NearKatNetworkClientCallback callback, final JSONObject response) {
            NearKatLogger.v("callback: " + callback + ", response: " + response);
            if (callback != null) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onSuccess(response);
                    }
                });
            }
        }
        
        private void failed(final NearKatNetworkClientCallback callback, final String response) {
            NearKatLogger.v("callback: " + callback + ", response: " + response);
            if (callback != null) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onFailure(response);
                    }
                });
            }
        }
        
    }
}
