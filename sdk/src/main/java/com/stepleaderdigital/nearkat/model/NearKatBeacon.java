package com.stepleaderdigital.nearkat.model;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

public class NearKatBeacon implements Parcelable {
    public static final String   KEY_BEACON          = "key_beacon";
    public static final String[] PROXIMITY           = { "unknown", "immediate", "near", "far" };
    
    public static final int      PROXIMITY_UNKNOWN   = 0;
    public static final int      PROXIMITY_IMMEDIATE = 1;
    public static final int      PROXIMITY_NEAR      = 2;
    public static final int      PROXIMITY_FAR       = 3;
    
    private String               address;
    private String               name;
    private String               proximityUUID;
    private int                  major;
    private int                  minor;
    private int                  txPower;
    private int                  proximity;
    private double               accuracy;
    private int                  rssi;
    private Location             location;
    
    public NearKatBeacon() {
        
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NearKatBeacon beacon = (NearKatBeacon) o;

        if (address != null ? !address.equals(beacon.address) : beacon.address != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return address != null ? address.hashCode() : 0;
    }

    public String getAddress() {
        return address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getProximityUUID() {
        return proximityUUID;
    }
    
    public void setProximityUUID(String proximityUUID) {
        this.proximityUUID = proximityUUID;
    }
    
    public int getMajor() {
        return major;
    }
    
    public void setMajor(int major) {
        this.major = major;
    }
    
    public int getMinor() {
        return minor;
    }
    
    public void setMinor(int minor) {
        this.minor = minor;
    }
    
    public int getTxPower() {
        return txPower;
    }
    
    public void setTxPower(int txPower) {
        this.txPower = txPower;
    }
    
    public int getProximity() {
        return proximity;
    }
    
    public void setProximity(int proximity) {
        this.proximity = proximity;
    }
    
    public double getAccuracy() {
        return accuracy;
    }
    
    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }
    
    public int getRssi() {
        return rssi;
    }
    
    public void setRssi(int rssi) {
        this.rssi = rssi;
    }
    
    public Location getLocation() {
        return location;
    }
    
    public void setLocation(Location location) {
        this.location = location;
    }
    
    private NearKatBeacon(Parcel in) {
        address = in.readString();
        name = in.readString();
        proximityUUID = in.readString();
        major = in.readInt();
        minor = in.readInt();
        txPower = in.readInt();
        proximity = in.readInt();
        accuracy = in.readDouble();
        rssi = in.readInt();
        location = in.readParcelable(getClass().getClassLoader());
    }
    
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(address);
        out.writeString(name);
        out.writeString(proximityUUID);
        out.writeInt(major);
        out.writeInt(minor);
        out.writeInt(txPower);
        out.writeInt(proximity);
        out.writeDouble(accuracy);
        out.writeInt(rssi);
        out.writeParcelable(location, 0);
    }
    
    public static final Creator<NearKatBeacon> CREATOR = new Creator<NearKatBeacon>() {
        public NearKatBeacon createFromParcel(Parcel in) {
            return new NearKatBeacon(in);
        }
        public NearKatBeacon[] newArray(int size) {
            return new NearKatBeacon[size];
        }
    };
    
    @Override
    public int describeContents() {
        return hashCode();
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Beacon [address: ").append(address)
            .append(", name: ").append(name)
            .append(", proximityUUID: ").append(proximityUUID)
            .append(", major: ").append(major)
            .append(", minor: ").append(minor)
            .append(", txPower: ").append(txPower)
            .append(", proximity: ").append(proximity)
            .append(", accuracy: ").append(accuracy)
            .append(", rssi: ").append(rssi)
            .append(", location: ").append(location)
            .append("]");
        return builder.toString();
    }
}
