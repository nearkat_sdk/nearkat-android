package com.stepleaderdigital.nearkat;

/**
 * Created by seandoherty on 1/13/15.
 */
public enum ServiceType {
    SANDBOX, PRODUCTION
}
