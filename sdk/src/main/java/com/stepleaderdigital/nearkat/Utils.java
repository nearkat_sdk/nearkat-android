package com.stepleaderdigital.nearkat;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.stepleaderdigital.nearkat.model.NearKatBeacon;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.util.List;
import java.util.Locale;

public class Utils {
    
    @SuppressLint({ "InlinedApi", "NewApi" })
    public static boolean isBluetoothLeSupported(Context context) {
        if (Build.VERSION.SDK_INT >= 18) {
            return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
        }
        return false;
    }
    
    @SuppressLint({ "InlinedApi", "NewApi" })
    public static BluetoothAdapter getBluetoothAdapter(Context context) {
        if (Build.VERSION.SDK_INT >= 18 && isBluetoothLeSupported(context)) {
            return BluetoothAdapter.getDefaultAdapter();
        }
        return null;
    }
    
    public static boolean isBluetoothEnabled(Context context) {
        boolean enabled = false;
        BluetoothAdapter adapter = getBluetoothAdapter(context);
        if (adapter != null) {
            enabled = adapter.isEnabled();
        }
        return enabled;
    }
    
    /**
     * Create initial json info required to obtain beacon setup.
     * @param context Context
     * @return json String
     */
    public static String getJsonInitInfo(Context context) {
        JSONObject json = getBaseJsonObject(context);
        try {
            json.put("version", Build.VERSION.RELEASE);
            json.put("locale", Locale.getDefault());
            json.put("supports_ble", isBluetoothLeSupported(context));
            
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    /**
     * Create initial json info required to obtain beacon setup.
     * @param context Context
     * @return json String
     */
    public static String getJsonAppInfo(Context context, List<String> appNames) {
        JSONObject json = new JSONObject();
        try {
            json.put("device_id", getDeviceId(context));
            json.put("app_ids", new JSONArray(appNames));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    /**
     * Creates json string for discovered beacon.
     * @param context
     * @return
     */
    public static String getJsonBeacon(Context context, NearKatBeacon beacon) {
        JSONObject json = getBaseJsonObject(context, beacon);
        try {
            JSONObject beaconCharacteristics = new JSONObject();
            beaconCharacteristics.put("name", beacon.getName());
            beaconCharacteristics.put("txpower", beacon.getTxPower());
            json.put("characteristics", beaconCharacteristics);
            json.put("beacon_uuid", beacon.getProximityUUID());
            json.put("beacon_major", beacon.getMajor());
            json.put("beacon_minor", beacon.getMinor());
            json.put("beacon_mac", beacon.getAddress());
            json.put("beacon_proximity", NearKatBeacon.PROXIMITY[beacon.getProximity()]);
            json.put("beacon_accuracy", beacon.getAccuracy());
            json.put("beacon_rssi", beacon.getRssi());
            
            Location location = beacon.getLocation();
            if (location != null) {
                JSONObject address = getJsonAddress(context, location.getLatitude(), location.getLongitude());
                if (address != null) {
                    json.put("address", address);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }
    
    /**
     * Creates crash info json
     * @param context Context
     * @param thread String
     * @param ex String
     * @return String
     */
    public static String getJsonCrash(Context context, String thread, String ex) {
        JSONObject json = getBaseJsonObject(context);
        try {
            json.put("version", Build.VERSION.RELEASE);
            json.put("supports_ble", isBluetoothLeSupported(context));
            
            JSONObject characteristics = new JSONObject();
            characteristics.put("thread", thread);
            characteristics.put("throwable", ex);
            characteristics.put("device", Build.MANUFACTURER + " " + Build.MODEL);
            json.put("characteristics", characteristics);
            
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }
    
    /**
     * Creates base JSONObject with basic device info with device based location.
     * @param context Context
     * @returnJSONObject
     */
    private static JSONObject getBaseJsonObject(Context context) {
        return getBaseJsonObject(context, null);
    }
    
    /**
     * Creates base JSONObject with basic device info with beacon based location.
     * @param context Context
     * @param beacon Beacon
     * @return JSONObject
     */
    private static JSONObject getBaseJsonObject(Context context, NearKatBeacon beacon) {
        JSONObject json = new JSONObject();
        try {
            
            json.put("device_id", getDeviceId(context));
            json.put("os", getOsName());
            json.put("bluetooth_enabled", isBluetoothEnabled(context));
            json.put("app_version", getAppVersion(context));
            
            JSONObject location = getLocationJson(context, beacon);
            if (location != null) {
                json.put("location", getLocationJson(context, beacon));
            }
            
            String adId = AdUtils.getAdvertisingId(context);
            if (TextUtils.isEmpty(adId) == false) {
                json.put("idfa", adId);
            }
            
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }
    
    private static JSONObject getLocationJson(Context context, NearKatBeacon beacon) {
        Location location = null;
        if (beacon != null) {
            location = beacon.getLocation();
        } else {
            location = getLastKnownLocation(context);
        }
        JSONObject jsonObject = new JSONObject();
        if (location != null) {
            try {
                jsonObject.put("lat", location.getLatitude());
                jsonObject.put("long", location.getLongitude());
                jsonObject.put("time", (System.currentTimeMillis() - location.getTime()) / 1000);
                jsonObject.put("altitude", location.getAltitude());
                jsonObject.put("accuracy", location.getAccuracy());
                jsonObject.put("provider", location.getProvider());
                return jsonObject;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    private static JSONObject getJsonAddress(Context context, double latitude, double longitude) {
        Address address = getAddress(context, latitude, longitude);
        if (address != null) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("street", address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "");
                jsonObject.put("city", address.getLocality());
                jsonObject.put("state", address.getAdminArea());
                jsonObject.put("zip", address.getPostalCode());
                jsonObject.put("country", address.getCountryName());
                return jsonObject;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    /**
     * Long process. Need to be run on worker thread.
     * @param context Context
     * @param latitude double
     * @param longitude double
     * @return Address
     */
    private static Address getAddress(Context context, double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() > 0) {
            return addresses.get(0);
        }
        return null;
    }
    
    private static String getOsName() {
        if (isKindle()) {
            return "kindle";
        }
        return "android";
    }
    
    private static boolean isKindle() {
        return Build.MANUFACTURER.toUpperCase(Locale.US).equals("AMAZON") && (Build.MODEL.toUpperCase(Locale.US).startsWith("KF") || Build.MODEL.toUpperCase(Locale.US).contains("KINDLE"));
    }
    
    private static String getAppVersion(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        return "unknown";
    }
    
    @SuppressLint("InlinedApi")
    private static String getDeviceId(Context context) {
        String deviceId = null;
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            deviceId = telephonyManager.getDeviceId();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (TextUtils.isEmpty(deviceId)) {
            try {
                if (Build.VERSION.SDK_INT >= 9) {
                    deviceId = Build.SERIAL;
                }
                if (deviceId == null) {
                    WifiManager manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                    WifiInfo wifiInfo = manager.getConnectionInfo();
                    deviceId = wifiInfo.getMacAddress();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        try {
            if (deviceId == null) {
                if (Build.VERSION.SDK_INT >= 3) {
                    deviceId = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        deviceId = createDeviceIdMD5Hash(deviceId);
        return deviceId;
    }
    
    public static String createDeviceIdMD5Hash(final String inputString) {
        String deviceId = inputString;
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(("android" + deviceId).getBytes());
            byte[] messageDigest = digest.digest();
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            }
            deviceId = hexString.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return deviceId;
    }
    
    public static Location getLastKnownLocation(Context context) {
        if (context != null) {
            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            if (locationManager != null) {
                return locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
        }
        return null;
    }

    public static Intent getServiceStartIntent(Context context, Integer scanInterval, Integer scanDuration) {
        Intent intent = new Intent("com.stepleaderdigital.nearkat.sharedservice.Scan");
        intent.putExtra(NearKat.BLUETOOTH_SCAN_DURATION_KEY, scanDuration);
        intent.putExtra(NearKat.BLUETOOTH_SCAN_INTERVAL_KEY, scanInterval);
        return intent;
    }
}
