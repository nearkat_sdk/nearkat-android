package com.stepleaderdigital.nearkat.sharedservice;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothAdapter.LeScanCallback;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;

import com.stepleaderdigital.nearkat.NearKat;
import com.stepleaderdigital.nearkat.NearKatLogger;
import com.stepleaderdigital.nearkat.Utils;
import com.stepleaderdigital.nearkat.model.NearKatBeacon;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

@SuppressLint("NewApi")
public class NearKatBluetoothScanService extends Service implements LeScanCallback {

    private Integer scanDuration;
    private Integer scanInterval;

    private Set<String> foundBeaconAddressSet = new HashSet<String>();
    
    private boolean            isRunning = false;
    private BluetoothAdapter   mBluetoothAdapter = null;
    
    private final Handler            mHandler          = new Handler();

    /**
     * Call to stop the current service.
     * @param startId int
     * @return int
     */
    private int stopService(int startId) {
        stopSelf(startId);
        return getServiceType();
    }
    
    /**
     * Returns service type.
     * @return int
     */
    private int getServiceType() {
        return START_STICKY;
    }
    
    /**
     * Schedule new service run.
     */
    private void scheduleRerun() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MILLISECOND, (int) scanInterval);
        
        Intent serviceIntent = Utils.getServiceStartIntent(this,scanInterval,scanDuration);
        PendingIntent pendingIntent = PendingIntent.getService(this, 0, serviceIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        
        AlarmManager alarmManager = (AlarmManager) getSystemService(Activity.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        loadFoundBeacons();
        scanDuration = intent.getIntExtra(NearKat.BLUETOOTH_SCAN_DURATION_KEY, 60000);
        scanInterval = intent.getIntExtra(NearKat.BLUETOOTH_SCAN_INTERVAL_KEY, 3000);

        if (isRunning) {
            NearKatLogger.v("Scanning service is already running, returning..");
            return stopService(startId);
        }

        NearKatLogger.v("Scanning service is not running yet, starting new service..");
        isRunning = true;
        
        mBluetoothAdapter = Utils.getBluetoothAdapter(this);
        
        if (Utils.isBluetoothLeSupported(this) == false || mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            NearKatLogger.v("Bluetooth LE not supported. Stopping scanning service.");
            return stopService(startId);
        }
        scheduleRerun();
        startBluetoothLEScan(startId);
        return getServiceType();
    }
    
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    
    @Override
    public void onDestroy() {
        storeFoundBeacons();
    }

    @Override
    public void onLeScan(BluetoothDevice bluetoothDevice, int rssi, byte[] scanRecord) {
        if (isBeacon(scanRecord)) {
            NearKatLogger.v("Beacon found");
            String msg = null;
            for (byte b : scanRecord) {
                msg += String.format("%02x", b);
            }
            msg = msg.toUpperCase(Locale.getDefault());
            
            StringBuilder uuidBuilder = new StringBuilder()
            .append(msg.substring(22, 30))
            .append("-")
            .append(msg.substring(30, 34))
            .append("-")
            .append(msg.substring(34, 38))
            .append("-")
            .append(msg.substring(38, 42))
            .append("-")
            .append(msg.substring(42, 54));
            
            String deviceAddress = bluetoothDevice.getAddress();
            String deviceName = bluetoothDevice.getName();
            String deviceUuid = uuidBuilder.toString();
            int deviceMajor = Integer.parseInt(msg.substring(54, 58), 16);
            int deviceMinor = Integer.parseInt(msg.substring(58, 62), 16);
            int deviceTxPower = scanRecord[29];
            double deviceAccuracy = calculateAccuracy(deviceTxPower, rssi);
            int deviceProximity = calculateProximity(deviceAccuracy);
            
            NearKatBeacon beacon = new NearKatBeacon();
            beacon.setAddress(deviceAddress);
            beacon.setName(deviceName);
            beacon.setProximityUUID(deviceUuid);
            beacon.setMajor(deviceMajor);
            beacon.setMinor(deviceMinor);
            beacon.setTxPower(deviceTxPower);
            beacon.setAccuracy(deviceAccuracy);
            beacon.setProximity(deviceProximity);
            beacon.setRssi(rssi);
            beacon.setLocation(Utils.getLastKnownLocation(this));
            
            if (!foundBeaconAddressSet.contains(beacon.getAddress())) {
                NearKatLogger.v("New beacon being registered: " + beacon);
                foundBeaconAddressSet.add(beacon.getAddress());

                NearKat.getInstance().sendDiscoveryOfBeacon(this,beacon);
            } else NearKatLogger.v("Beacon already sent");
        }
    }
    
    private void startBluetoothLEScan(final int startId) {
        //TODO: check for 5.0 capability and use new scanning code rather than existing scanning code

        //Setup handler to delay stopping of service until after the scanDuration has elapsed
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mBluetoothAdapter.stopLeScan(NearKatBluetoothScanService.this);
                stopService(startId);
            }
        }, scanDuration);
        mBluetoothAdapter.startLeScan(NearKatBluetoothScanService.this);
    }
    
    private static boolean isBeacon(byte[] scanRecord) {
        int startByte = 2;
        while (startByte <= 5) {
            if (((int) scanRecord[startByte + 2] & 0xff) == 0x02 && ((int) scanRecord[startByte + 3] & 0xff) == 0x15) {
                return true;
            }
            startByte++;
        }
        return false;
    }

    //Using guesstimated formula from iOS: Saved from original version - SD
    private static double calculateAccuracy(int txPower, double rssi) {
        double accuracy = -1;
        if (rssi != 0) {
            double ratio = rssi * 1.0 / txPower;
            if (ratio < 1.0) {
                accuracy = Math.pow(ratio, 10);
            } else {
                accuracy = (0.89976) * Math.pow(ratio, 7.7095) + 0.111;
            }
        }
        return Double.valueOf(new DecimalFormat("#.00").format(accuracy));
    }
    
    private static int calculateProximity(double accuracy) {
        if (accuracy < 0) {
            return NearKatBeacon.PROXIMITY_UNKNOWN;
        }
        if (accuracy < 0.5) {
            return NearKatBeacon.PROXIMITY_IMMEDIATE;
        }
        if (accuracy <= 4.0) {
            return NearKatBeacon.PROXIMITY_NEAR;
        }
        return NearKatBeacon.PROXIMITY_FAR;
    }
    //We store and load founds beacons from shared preferences so that the values are maintained across Service instances
    private void storeFoundBeacons() {
        SharedPreferences settings = this.getSharedPreferences(NearKat.FOUND_BEACONS_PREFERENCE_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putStringSet(NearKat.FOUND_BEACONS_PREFERENCE_KEY, this.foundBeaconAddressSet);
        editor.commit();
    }

    private void loadFoundBeacons() {
        SharedPreferences sharedPreferences = this.getSharedPreferences(NearKat.FOUND_BEACONS_PREFERENCE_NAME, 0);
        this.foundBeaconAddressSet = sharedPreferences.getStringSet(NearKat.FOUND_BEACONS_PREFERENCE_KEY, new HashSet<String>());
    }
}
