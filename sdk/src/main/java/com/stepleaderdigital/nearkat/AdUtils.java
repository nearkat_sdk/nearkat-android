package com.stepleaderdigital.nearkat;

import java.io.IOException;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

public class AdUtils {
    
    private static final String PREFS_NAME = "adid";
    private static final String KEY_ID     = "key_adid";
    
    /**
     * Retrieves advertising id if not restricted.
     * @param context Context
     */
    public static void setupAdvertisingId(final Context context) {
        if (context == null) {
            return;
        }
        new Thread(new Runnable() {
            public void run() {
                Info adInfo = null;
                try {
                    adInfo = AdvertisingIdClient.getAdvertisingIdInfo(context);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (adInfo != null) {
                    String adid = "";
                    if (adInfo.isLimitAdTrackingEnabled() == false) {
                        adid = adInfo.getId();
                    }
                    SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString(KEY_ID, adid);
                    editor.commit();
                }
            }
        }).start();
    }
    
    /**
     * Returns Advertising Id if not restricted.
     * @param context
     * @return
     */
    public static String getAdvertisingId(final Context context) {
        if (context == null) {
            return "";
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
        return sharedPreferences.getString(KEY_ID, "");
    }
    
}
