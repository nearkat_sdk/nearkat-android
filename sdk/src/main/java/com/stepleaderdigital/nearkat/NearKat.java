package com.stepleaderdigital.nearkat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;

import com.stepleaderdigital.nearkat.model.NearKatBeacon;
import com.stepleaderdigital.nearkat.network.NearKatNetworkClient;
import com.stepleaderdigital.nearkat.network.NearKatNetworkClientCallback;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by seandoherty on 1/13/15.
 */
public class NearKat {
    private static NearKat sharedInstance = null;

    private String apiKey = null;
    private String apiBaseURL = NEARKAT_API_BASE_PRODUCTION;
    private Boolean isDebug = false;
    private List<String> debugUUIDs = new ArrayList<String>();
    private List<String> personas = new ArrayList<String>();

    private static final String NEARKAT_API_BASE_PRODUCTION = "http://sdk.neark.at";
    private static final String NEARKAT_API_BASE_SANDBOX = "http://sandboxsdk.neark.at";
    public static final String BLUETOOTH_SCAN_INTERVAL_KEY = "BLUETOOTH_SCAN_INTERVAL_KEY";
    public static final String BLUETOOTH_SCAN_DURATION_KEY = "BLUETOOTH_SCAN_DURATION_KEY";
    public static final String FOUND_BEACONS_PREFERENCE_NAME = "NearKat_Beacon_Preferences";
    public static final String FOUND_BEACONS_PREFERENCE_KEY = "FOUND_BEACONS";
    private NearKat(){
    }

    public static synchronized NearKat getInstance() {
        if (sharedInstance == null)
            sharedInstance = new NearKat();
        return sharedInstance;
    }

    public void start(final Context context) {
        if (context == null)
            throw new RuntimeException("Application Context passed into NearKat must not be null");
        if (this.apiKey == null)
            throw new RuntimeException("The NearKat SDK Requires an API Key to start");

        // Register Device
        NearKatNetworkClient.registerDevice(context, new NearKatNetworkClientCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                //Handle response to registration
                JSONArray personasJSONArray = response.optJSONArray("personas");
                if (personasJSONArray != null)
                    setPersonasWithJSON(personasJSONArray);

                // After Registration success, scan for installed apps and start bluetooth scanning
                Boolean discoveryEnabled = response.optBoolean("discovery_enabled");
                if (discoveryEnabled != null && discoveryEnabled){
                    sendInstalledApps(context);

                    Integer scanInterval = response.optInt("scan_interval") * 1000;
                    Integer scanLength = response.optInt("scan_length") * 1000;
                    startBeaconScanning(context, scanInterval,scanLength);
                }
            }

            @Override
            public void onFailure(String response) {
                NearKatLogger.v("error " + response);
            }
        });
        clearBeaconsFound(context);
    }

    private void clearBeaconsFound(Context context) {
        SharedPreferences settings = context.getSharedPreferences(NearKat.FOUND_BEACONS_PREFERENCE_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putStringSet(NearKat.FOUND_BEACONS_PREFERENCE_KEY, new HashSet<String>());
        editor.commit();
    }

    private void sendInstalledApps(Context context) {
        List<String> apps = getInstalledApps(context, false);
        NearKatNetworkClient.sendInstalledApps(context, apps, new NearKatNetworkClientCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                JSONArray personasJSONArray = response.optJSONArray("personas");
                if (personasJSONArray != null)
                    setPersonasWithJSON(personasJSONArray);
            }

            @Override
            public void onFailure(String response) {
                NearKatLogger.v("error " + response);
            }
        });
    }

    private void startBeaconScanning(Context context, Integer scanInterval, Integer scanDuration) {
        if (Utils.isBluetoothLeSupported(context)) {
            Intent intent = Utils.getServiceStartIntent(context, scanInterval, scanDuration);
            context.startService(intent);
        }
    }

    public void sendDiscoveryOfBeacon(Context context, NearKatBeacon beacon) {
        NearKatNetworkClient.sendNotificationOfBeacon(context, beacon, new NearKatNetworkClientCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                JSONArray personasJSONArray = response.optJSONArray("personas");
                if (personasJSONArray != null)
                    setPersonasWithJSON(personasJSONArray);
            }

            @Override
            public void onFailure(String response) {
                NearKatLogger.v("error " + response);
            }
        });
    }

    /**
     * Get application package name
     * @param context Context
     * @return String
     */
    private String getAppPackageName(Context context) {
        if (context != null) {
            return context.getApplicationContext().getPackageName();
        }
        return "";
    }

    private void setPersonasWithJSON(JSONArray personasJSONArray) {
        List<String> personas = new ArrayList<>();
        for (int i=0; i < personasJSONArray.length(); i++) {
            personas.add(personasJSONArray.optString(i));
        }
        this.personas = personas;
    }

    public void setAPIKey(String apiKey){
        this.apiKey = apiKey;
    }

    public void setServiceType(ServiceType serviceType) {
        if (serviceType == ServiceType.SANDBOX) {
            this.apiBaseURL = NEARKAT_API_BASE_SANDBOX;
        } else this.apiBaseURL = NEARKAT_API_BASE_PRODUCTION;
    }

    public void setDebug(Boolean isDebug) {
        this.isDebug = isDebug;
    }

    public void setDebugUUIDs(List<String> debugUUIDs) {
        this.debugUUIDs = debugUUIDs;
    }

    public List<String> getPersonas(){
        return personas;
    }

    public String getAPIKey() {
        return apiKey;
    }

    public String getAPIBaseURL() {
        return apiBaseURL;
    }

    public Boolean getIsDebug() {
        return isDebug;
    }

    /**
     * Methods for getting installed apps to server
     */
    private List<String> getInstalledApps(Context context, boolean getSysPackages) {
        ArrayList<String> res = new ArrayList<String>();
        List<PackageInfo> packs = context.getPackageManager().getInstalledPackages(0);
        for(int i=0;i<packs.size();i++) {
            PackageInfo p = packs.get(i);
            if ((!getSysPackages) && (p.versionName == null)) {
                continue ;
            }
            String appName = null;
            try {
                appName = p.applicationInfo.loadLabel(context.getPackageManager())
                        .toString();
            } catch (Exception e) {
                NearKatLogger.v(e);
            }
            if (appName != null)
                res.add(appName);
        }
        return res;
    }
}
