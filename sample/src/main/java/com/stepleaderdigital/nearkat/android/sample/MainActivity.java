package com.stepleaderdigital.nearkat.android.sample;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.stepleaderdigital.nearkat.NearKatLogger;
import com.stepleaderdigital.nearkat.NearKat;
import com.stepleaderdigital.nearkat.ServiceType;

import java.util.List;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Create Nearkat configuration.
        NearKat nearKat = NearKat.getInstance();
        nearKat.setAPIKey("53beb58e5e0a864201e61c56");
        nearKat.setServiceType(ServiceType.PRODUCTION);
        nearKat.setDebug(true);

        //Start Nearkat service. This will start start discovering beacons.
        nearKat.start(getApplicationContext());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        List<String> personas = NearKat.getInstance().getPersonas();
        NearKatLogger.v(personas);

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
